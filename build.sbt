import sbt.Keys._

organization := "nl.bluesoft"

name := "hello-streams"

version := "1.0.0"

scalaVersion := "2.11.7"

resolvers += "Local Maven Repository" at "file://"+Path.userHome.absolutePath+"/.m2/repository"

credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")

libraryDependencies += "com.scalawilliam" %% "xs4s" % "0.2-SNAPSHOT"

resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

libraryDependencies ++=
  {
    val akkaV = "2.3.9"
    val streamV = "1.0"
    Seq(
      "com.typesafe.akka"       %% "akka-actor"                           % akkaV,
      "com.typesafe.akka"       %% "akka-slf4j"                           % akkaV,
      "com.typesafe.akka"       %% "akka-stream-experimental"             % streamV,
      "com.typesafe.akka"       %% "akka-http-core-experimental"          % streamV,
      "com.typesafe.akka"       %% "akka-http-experimental"               % streamV,
      "com.typesafe.akka"       %% "akka-http-spray-json-experimental"    % streamV,
      "com.typesafe.akka"       %% "akka-http-xml-experimental"           % streamV,
      "org.scalatest"           %% "scalatest"                            % "2.2.4" % "test",
      "com.typesafe.akka"       %% "akka-testkit"                         % akkaV % "test",
      "com.typesafe.akka"       %% "akka-http-testkit-experimental"       % streamV % "test",
      "com.typesafe.akka"       %% "akka-stream-testkit-experimental"     % streamV % "test"
    )
  }

autoCompilerPlugins := true

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

publishMavenStyle := true

publishArtifact in Test := false

fork := true

parallelExecution in test := false

