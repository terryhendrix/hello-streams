package nl.scalaterry.akka.stream

import java.io.{BufferedOutputStream, FileOutputStream}

import akka.actor.ActorSystem
import akka.http.scaladsl.marshallers.xml.ScalaXmlSupport
import akka.stream.ActorMaterializer
import akka.stream.scaladsl._
import org.scalatest.concurrent.{PatienceConfiguration, ScalaFutures}
import org.scalatest.time.{Span, Seconds}
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, FlatSpecLike, Matchers}

import scala.util.Try

class FileStreamingTest extends FlatSpecLike with ScalaXmlSupport with Matchers with BeforeAndAfter with BeforeAndAfterAll with ScalaFutures
{
  implicit val system = ActorSystem("IntegerStream")
  implicit val ec = system.dispatcher
  implicit val mat = ActorMaterializer()

  case class Data(firstName:String, lastName:String)


  "A Stream of Data" should "be written as xml" in {
    val source = Source(someStreamOfData(1000000))

    val fileOutput = new BufferedOutputStream(new FileOutputStream("test-file.xml"))

    val fileWriter = Flow[String].map { xml ⇒
      Try(fileOutput.write(xml.getBytes))
    }

    val xmlHeader = """<camt xmlns="http://some-namespace"
                      |      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                      |      xsi:schemaLocation="http:/some-namespace/def.xsd">""".stripMargin
    val xmlFooter = """</camt>""".stripMargin

    val marshaller = Flow[Data].map { data ⇒
      <data>
        <firstName>{data.firstName}</firstName>
        <firstName>{data.lastName}</firstName>
      </data>.toString
    }

    val failureCollector = Sink.fold[Seq[Throwable], Try[Unit]](Nil) { (seq, t) ⇒
      t.map(_ ⇒ seq).recover {
        case ex:Throwable ⇒ seq :+ ex
      }.get
    }


    fileOutput.write(xmlHeader.getBytes)
    source.via(marshaller).via(fileWriter).runWith(failureCollector).map { failures ⇒
      fileOutput.write(xmlFooter.getBytes)
      fileOutput.flush()
      fileOutput.close()

      failures.foreach(System.err.println)
    }.futureValue(PatienceConfiguration.Timeout(Span(15, Seconds)))
  }

  def someStreamOfData(size:Int) = Stream.fill(size)(Data("Terry", "Hendrix"))

}
