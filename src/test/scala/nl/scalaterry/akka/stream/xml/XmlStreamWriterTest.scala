package nl.scalaterry.akka.stream.xml

import java.io.File
import akka.actor.ActorSystem
import akka.http.scaladsl.marshallers.xml.ScalaXmlSupport
import akka.stream.ActorMaterializer
import akka.stream.scaladsl._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, FlatSpecLike, Matchers}
import scala.xml.Elem

case class Data(firstName:String, lastName:String)

class XmlStreamWriterTest extends FlatSpecLike with ScalaXmlSupport with Matchers with BeforeAndAfter with BeforeAndAfterAll with ScalaFutures
                       with XmlStreamWriter[Data]
{
  implicit val system = ActorSystem("IntegerStream")
  implicit val ec = system.dispatcher
  implicit val mat = ActorMaterializer()

  override def rootElement: String = "persons"

  override def rootAttributes: Map[String, String] = Map.empty

  "A stream writer" should " be written to a file " in {
    implicit def marshaller(data:Data): Elem = {
      <data>
        <firstName>{data.firstName}</firstName>
        <firstName>{data.lastName}</firstName>
      </data>
    }

    val source = Source(Stream.fill(100000)(Data("Terry", "Hendrix")))
    source.to("test-streaming.xml")
  }

  it should " have another signature " in {
    implicit def marshaller(data:Data): Elem = {
      <data>
        <firstName>{data.firstName}</firstName>
        <firstName>{data.lastName}</firstName>
      </data>
    }

    val source = Source(Stream.fill(100000)(Data("Terry", "Hendrix")))
    source.to(new File("test-streaming.xml"))
  }

}
