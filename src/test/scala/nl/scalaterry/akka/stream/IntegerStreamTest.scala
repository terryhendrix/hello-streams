package nl.scalaterry.akka.stream

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, FlatSpecLike, Matchers}

/**
 * Created by terryhendrix on 07/08/15.
 */
class IntegerStreamTest extends FlatSpecLike with Matchers with BeforeAndAfter with BeforeAndAfterAll with ScalaFutures
{
  implicit val system = ActorSystem("IntegerStream")
  implicit val mat = ActorMaterializer()

  "Some basic stuff" should " do some with integers " in {
    val source = Source(1 to 4)
    val sink = Sink.fold[Int, Int](0)(_ + _)

    // connect the Source to the Sink, obtaining a RunnableFlow
    val sum = source.runWith(sink).futureValue
    println(s"Sum: ${sum}")
    sum shouldBe 10
  }

  override def afterAll() = {
    system.shutdown()
    system.awaitTermination()
  }

}
