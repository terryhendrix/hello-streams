package nl.scalaterry.akka.stream.xml
import java.io.{File, BufferedOutputStream, FileOutputStream}

import akka.stream.Materializer
import akka.stream.scaladsl.{Flow, Sink, Source}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try
import scala.xml.Elem

trait XmlStreamWriter[T]
{
  def rootElement:String
  def rootAttributes:Map[String,String]

  val failureCollector = Sink.fold[Seq[Throwable], Try[Unit]](Nil) { (seq, t) ⇒
    t.map(_ ⇒ seq).recover {
      case ex:Throwable ⇒ seq :+ ex
    }.get
  }

  private def header = s"""<$rootElement ${rootAttributes.asString()}>""".stripMargin
  private def footer = s"""</$rootElement>""".stripMargin

  private implicit class ExtendedAttributes(attributes:Map[String,String]) {
    def asString() = "" //TODO: Implement
  }

  protected implicit class SourceToXmlFile(source:Source[T, Unit])
  {
    def to(file:String) = to(new File(file))

    def to(file:File)(implicit marshallFunction: T ⇒ Elem, mat:Materializer, ec:ExecutionContext):Future[Iterable[Throwable]] = {
      val fileOutput = new BufferedOutputStream(new FileOutputStream(file))
      val fileWriter = Flow[Array[Byte]].map { bytes ⇒ Try(fileOutput.write(bytes)) }
      val marshaller = Flow[T].map { data ⇒ marshallFunction(data).toString.getBytes }
      fileOutput.write(header.getBytes)
      source.via(marshaller).via(fileWriter).runWith(failureCollector).map { failures ⇒
        fileOutput.write(footer.getBytes)
        fileOutput.flush()
        fileOutput.close()
        failures
      }
    }
  }
}
